set mouse=a         " enable mouse in all the different vim modes
set wildmenu        " visual autocomplete for command menu

"#################
" Spaces and Tabs
"#################
set shiftwidth=4    " number of visual spaces when indenting with >> and <<
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are replaced by a number of spaces; see tabstop=number

"#################
" Filetype specific settings
"#################
augroup filetype_yaml
    autocmd!
    " Call ansible-doc when using Shift+k on a module name (replaced by Coc)
    "autocmd BufNewFile,BufRead *.{yaml,yml} set keywordprg=ansible-doc
    autocmd FileType yaml setlocal softtabstop=2 tabstop=2 sw=2 expandtab " foldmethod=indent
augroup END

augroup filetype_sh
    autocmd!
    autocmd FileType sh setlocal softtabstop=2 tabstop=2 sw=2 expandtab
augroup END

"augroup filetype_text
"    autocmd!
"    " Enable spell checking for files containing text
"    autocmd BufNewFile,BufRead *.{md,txt,tex} set spell " see tutorial https://jeromebelleman.gitlab.io/posts/publishing/vimspell/
"augroup END

" ##############
" Finding Files
" ##############
" search down into subfolders
" Provides tab-completion for all file-related tasks
set path+=**
" Now we can:
" - Hit tab to :find by partial match
" - Use * to make it fuzzy
" Things to consider:
" - :b lets you autocomplete any open buffer


" Tweaks for file browsing
let g:netrw_banner = 0 " disable banner
let g:netrw_winsize = 80 " set netrw size to 20%
let g:netrw_browser_split = 4 " open in vsplit
let g:netrw_alto = 1 " open hsplits to the bottom
let g:netrw_altv = 1 " open vsplits to the right
let g:netrw_liststyle = 3 " tree view
"let g:netrw_list_hide=netrw_gitignore#Hide()
"let g:netrw_list_hist.=',\(^\|\s\s)\zs\.\S\+'

"set undodir=~/.vim/tmp/undo//     " undo files
set backupdir=~/.vim/tmp/backup//  " backup files
set directory=~/.vim/tmp/swap//    " swap files, setting this prevents
"detection of multiple users editing the same file
"set noswapfile

" Show relative line numbers + number of current line for faster navigation
"set relativenumber
"set number

" use F1 key to go to previous tab
nnoremap <F1> :tabp<C-M>
" use F2 key to go to next tab
nnoremap <F2> :tabn<C-M>

" use F4 key to toggle line numbers
"nnoremap <F4> :set number! relativenumber!<CR>

" set leader key to space
let mapleader = " "

" enable keymappings of vimspector
" note: this binds most of the function keys
let g:vimspector_enable_mappings='HUMAN'


"###########
" UI Config
"###########
set noruler         " disable ruler to enable statusline
"set ruler			" enable ruler showing cursor's position
set showmatch		" show matching parantheses/brackets [{()}] when cursor is over them
set showcmd         " show current command while it is being typed
syntax on           " enable syntax highlighting
filetype on			" enable filetype detection for syntax highlighting, auto-indent and so on...
filetype plugin on	" enable filetype plugin
filetype indent on	" load filetype-specific indent files

set spelllang=en_us,de_de

" Which git branch am I on?
function! GitBranch()
    silent return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

augroup GitStatus
    autocmd!
    " prevent undefiend variable in netrw
    autocmd FileType netrw let b:git_branch = ''
    autocmd BufEnter * let b:git_branch = StatuslineGit()
augroup END

" TODO: Find solution that works with both light and dark terminals
"hi StatusLine ctermbg=8
"hi StatusLineNC ctermbg=8

set statusline=
set statusline+=%7*\[%n]                                            " buffer number
set statusline+=%{b:git_branch}                                     " current git branch
set statusline+=\ %f                                                " current file
set statusline+=\ %r                                                " read-only flag
set statusline+=%m                                                  " file modified flag
set statusline+=\ %{coc#status()}%{get(b:,'coc_current_function','')}
set statusline+=%=                                                  " right-alignment
set statusline+=\ %y                                                " filetype
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}          " encoding (utf-8 etc.)
set statusline+=\[%{&fileformat}\]                                  " lineending (dos/unix/mac)
set statusline+=\ %p%%                                              " relative location
set statusline+=\ %l:%c                                             " cursor position
set statusline+=\                                                   " margin
"
set laststatus=2

" show number of matches when using search
set shortmess-=S

" highlighting of trailing white spaces
highlight ExtraWhitespace ctermbg=darkred guibg=darkred
augroup trailing_spaces
    autocmd!
    autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
    autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhitespace /\s\+$/
    autocmd BufWinLeave * call clearmatches()
augroup END

set incsearch " live-preview for searches

" Plugin management
packadd! surround

" vim-plug
call plug#begin()
Plug 'https://github.com/neoclide/coc.nvim', {'tag': 'v0.0.82', 'do':':CocInstall @yaegassy/coc-ansible coc-clangd coc-python coc-sh coc-json coc-snippets coc-docker coc-markdownlint coc-sql coc-yaml coc-rust-analyzer coc-toml'}
Plug 'https://github.com/junegunn/fzf'
Plug 'https://github.com/junegunn/fzf.vim.git'
Plug 'https://github.com/pearofducks/ansible-vim.git'
Plug 'https://github.com/HiPhish/jinja.vim.git'
Plug 'https://github.com/dense-analysis/ale.git'
Plug 'https://github.com/mbbill/undotree.git', { 'do': ':helptags ~/.vim/plugged/undotree/doc' }
Plug 'https://github.com/puremourning/vimspector.git'
Plug 'https://github.com/tpope/vim-fugitive.git'

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting


" Coc
" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" coc specific settings
nmap <silent> ga <Plug>(coc-codeaction-line)
" required for @yaegassy/coc-ansible
let g:coc_filetype_map = {
  \ 'yaml.ansible': 'ansible',
  \ }

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> to scroll float windows/popups
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

" CocSearch is useful to refactor code where the language server does not
" provide refatoring features
" this keybinding allows for refactoring of the current word under the cursors
nnoremap <leader>rf :CocSearch <C-r><C-w><CR>

" Undotree
nnoremap <leader>ut :UndotreeToggle<CR>

" fuzzy file search using fzf.vim
nnoremap <leader>fs :tabe<CR>:FZF<CR>
" fuzzy code search using fzf.vim and ripgrep
nnoremap <leader>cs :Rg<CR>
" fuzzy buffer lines search using fzf.vim
nnoremap <leader>bs :BLines<CR>
" fuzzy git commit search using fzf.vim
nnoremap <leader>gcs :BCommits<CR>

" Vimspector
" for normal mode - the word under the cursor
nmap <Leader>di <Plug>VimspectorBalloonEval
" for visual mode, the visually selected text
xmap <Leader>di <Plug>VimspectorBalloonEval
nmap <Leader>db <Plug>VimspectorBreakpoints

command! BuildAndDebug :%update | :make build_debug | :call vimspector#Launch()
nnoremap <leader>ddb :BuildAndDebug<CR>

" ALE
let g:ale_lint_on_enter = 0
let g:ale_lint_on_save = 1
nmap <silent> <C-e> <Plug>(ale_next_wrap)
