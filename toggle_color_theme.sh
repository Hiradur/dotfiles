#!/bin/sh

# toggle_color_theme.sh
# Original author: Hiradur
# License: CC0

# This script toggles between light and dark theme for GTK, QT and specific applications

########################################################################
# Print message with timestamp to stderr
# Globals:
#   None
# Arguments:
#   messages to be printed
########################################################################
print_error() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')] $*" >&2
}

########################################################################
# Change theme of kitty terminal emulator if kitty command is available
# Globals:
#   None
# Arguments:
#   String of the color theme to change to, a string
########################################################################
change_kitty_theme() {
    if which kitty >/dev/null; then
        kitty +kitten themes --reload-in=all "$1"
    fi
}

########################################################################
# Change theme of QT5 applications
# Globals:
#   None
# Arguments:
#   scheme to change to, either 'dark' or 'light'
########################################################################
toggle_qt5_theme() {
    if [ "$1" = 'dark' ]; then
        sed -i 's/style=Adwaita/style=Adwaita-Dark/g' "${HOME}/.config/qt5ct/qt5ct.conf"
    elif [ "$1" = 'light' ]; then
        sed -i 's/style=Adwaita-Dark/style=Adwaita/g' "${HOME}/.config/qt5ct/qt5ct.conf"
    else
        print_error "invalid argument for toggle_qt5_theme"
    fi
}

# for GTK >= 4 use the color-scheme key to indicate color-scheme preference
# check if color-scheme key is available
if gsettings get org.gnome.desktop.interface color-scheme >/dev/null; then
    if [ "$(gsettings get org.gnome.desktop.interface color-scheme)" = "'prefer-dark'" ]; then
        gsettings set org.gnome.desktop.interface color-scheme 'prefer-light'
        toggle_qt5_theme 'light'
        change_kitty_theme "Tango Light"
    else
        gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
        toggle_qt5_theme 'dark'
        change_kitty_theme "Tango Dark"
    fi
# for GTK <4 change themes
else
    if [ "$(gsettings get org.gnome.desktop.interface gtk-theme)" = "'Adwaita-dark'" ]; then
        gsettings set org.gnome.desktop.interface gtk-theme "Adwaita"
        toggle_qt5_theme 'light'
        change_kitty_theme "Tango Light"
    else
        gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"
        toggle_qt5_theme 'dark'
        change_kitty_theme "Tango Dark"
    fi
fi
