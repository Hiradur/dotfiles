# dotfiles

My personal dotfiles, mostly for a terminal-oriented workflow. I don't care too much about appearance, the focus lies on enabling a workflow that is efficient to me.

## Usage
### Install software
On Debian, run:
```sh
sudo apt install adwaita-qt qgnomeplatform-qt5 \
kitty \
zsh zsh-syntax-highlighting zsh-autosuggestions zsh-antigen zoxide \
vim-gtk3 \
bat ripgrep fzf exa \
yamllint ansible-lint shellcheck jsonlint \
htop btop \
ranger highlight \
mpv cmus ncmpcpp \
npm nodejs \
fonts-inconsolata

# create directories for vim
mkdir -p ~/.vim/tmp/backup
mkdir -p ~/.vim/tmp/swap

# install plugin manager for vim to autoinstall plugins on first vim invocation
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

```

### Install optional CLI tools
```sh
sudo apt install asciinema bmon entr fd-find inxi net-tools gdu rmlint tree wavemon gita tig lsof apparmor-utils s-tui
```

### Install optional GUI tools
```sh
sudo apt install meld gimp inkscape libreoffice pdfchain keepassxc
```

### Install software development tools
```sh
sudo apt install \
    clangd
```

### Install dotfiles

```sh
git clone https://gitlab.com/Hiradur/dotfiles.git
cd dotfiles
stow -t ~ */

# let vim install the plugins
vim -c PlugInstall
```

### Advanced configuration (optional)
#### GNOME Desktop configuration
To configure the GNOME desktop to my liking, run
```sh
./configure_gnome_desktop.sh
```

#### Use keyboard shortcut to quickly toggle between light and dark mode for all applications
Bind a keyboard shortcut in your desktop environment to `toggle_color_theme.sh` or run the script directly when needed.
